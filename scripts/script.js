if (typeof window !== "undefined") {
    window.addEventListener("load", function () {

        function createNewUser() {
            let userName = prompt("Enter user's name");
            while (userName.trim() === "" || !userName || !isNaN(+userName)) {
                userName = prompt("Please re-enter user's name");
            }
            let userSecondName = prompt("Enter user's second name");
            while (userSecondName.trim() === "" || !userName || !isNaN(+userName)) {
                userSecondName = prompt("Please re-enter user's second name");
            }
            return {
                _firstName: userName,
                _lastName: userSecondName,
                get firstName() {
                    return this._firstName
                },
                get lastName() {
                    return this._lastName
                },
                setFirstName: function (newName) {
                    this._firstName = newName;
                },
                setLastName: function (newLastName) {
                    this._lastName = newLastName;
                },
                getLogin: function () {
                    return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`
                }
            };
        }

        let newUser = createNewUser();
        console.log(newUser.getLogin())

    });
}
